<?php
   $productname = $productprice = $productimg = null;
   $id = $_GET['type'];
    $getImg = $id;
    function fakePath($fakePath){
        $fakePath = explode("fakepath\\", $fakePath);
        return $fakePath;
    }
    function uploadFile(){
        $file_name = $_FILES['file']['name'];
        $file_type = $_FILES['file']['tmp_name'];
        move_uploaded_file($file_type, '../images/'. $file_name);  
    } 
     function getImgCurrent($id){
         $connect = new mysqli('localhost', 'root', '', 'Shoepage1');
        
            $sql = 'select thumbail from product where id = '.$id;
            $result = mysqli_query($connect, $sql);
            $data = mysqli_fetch_assoc($result);
            mysqli_close($connect);
            return $data['thumbail'];   
    }
   $getImgCurrent = getImgCurrent($getImg);

        if(isset($_POST['showimg'])){
            if(($_FILES['file']['name'])!=''){
                uploadFile();
                $getImgCurrent = $_FILES['file']['name'];
            }
            $_FILES['file']['name'] =  $getImgCurrent;
        }

        $connect = new mysqli('localhost', 'root', '', 'Shoepage1');
        if(isset($_POST['submit'])){
            if(($_POST['productname'])!=''){
                $productname = $_POST['productname'];
                $sql  = 'update product set title = "'.$productname.'" where id = '.$id;
                mysqli_query($connect, $sql);
            }
            
            if(isset($_POST['productprice']) && $_POST['productprice'] != 0){
                $productprice = $_POST['productprice'];
                $sql  = 'update product set price = "'.$productprice.'" where id = '.$id;
                mysqli_query($connect, $sql);
            }
            
            if(($_FILES['file']['name'])!=''){
                uploadFile();
                $productimg = $_FILES['file']['name'];
                $sql  = 'update product set thumbail = "'.$productimg.'" where id = '.$id;
                mysqli_query($connect, $sql);
            }
            header( "Location: edit.php?type=1 ");
        }
        mysqli_close($connect);

        if(isset($_POST['cancel'])){
            header( "Location: edit.php?type=1 ");
        }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EditDetail</title>
    <link rel="stylesheet" href="../bootstraps/bootstrap.min.css">
    <link rel="stylesheet" href="../js/bootstrap.min.js">
    <link rel="stylesheet" href="../css/home.css">
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/edit.css">
</head>
<body>
<div class="main-content" style="background-image: url(../images/pic9.jpg); height:100vh">
<div class="content-form">
            <form action="" method="post" enctype="multipart/form-data">
                <h2>Edit Product</h2>

                <div class="row">
                    <div class="col-md-6 form-edit-image">
                        <?php echo '  <img src="../images/'.$getImgCurrent .'" alt="" id="img">' ?>
                        <div class="form-group">
                            <input type="file" name="file" id="" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                        <button class="btn btn-show btn-primary" name="showimg">Show newimage</button>
          
                      </div>
                    <div class="col-md-6 form-edit-info">
                        <div class="form-group">
                          <label for="" class="text-uppercase">ProductName</label>
                          <input type="text" name="productname" id="" class="form-control" placeholder="" aria-describedby="helpId">
                           </div>
                        <div class="form-group">
                            <label for="" class="text-uppercase">ProductPrice</label>
                            <input type="number" name="productprice" id=""  class="form-control" placeholder="" aria-describedby="helpId">
                           </div>
                          <div class="btnSubmit">
                            <button class="btn btn-confirm btn-primary"  name="submit">Confirm</button>
                            <button class="btn btn-cancel btn-primary" name="cancel">Cancel</button>           
                        </div>
                    </div>
                </div>
            </form>
                  
        </div>
</div>
<script src="../js/jquery-3.5.1.min.js"></script>
</body>
</html>