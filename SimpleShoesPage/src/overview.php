<?php
    require_once 'handling.php';
    //create products
    $typeUser = $_GET['type'];
?>
<!DOCTYPE html>
<html lang="en">
<head>   
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OverviewPage</title>
   
    <link rel="stylesheet" href="../bootstraps/bootstrap.min.css">
    <link rel="stylesheet" href="../js/bootstrap.min.js">
    <link rel="stylesheet" href="../css/slick.css">
    <link rel="stylesheet" href="../css/pageschild.css">
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class="header">
        <div class="header-title">SHOES SHOP</div>
        <div class="header-nav">
            <ul class="header-nav-list">
                <li class="header-nav-item"> <a href="#">Overview</a> </li>
                <li class="header-nav-item create create-link"> <a href="#">Create</a></li>
                <li class="header-nav-item edit-link"> <a href="#">Edit</a> </li>
                <li class="header-nav-item delete-link"> <a href="#">Delete</a> </li>
                <li class="header-nav-item"> <a href="../src/home.php">Log out</a> </li>
            </ul>
        </div>
    </div>
    <div class="content">
        <div class="content-listcategories">
            <h2>MEN'S</h2>
            <ul class="listcategories">
                <li class="listcategories-tem">Shoes
                    <ul>
                        <li>Chukka Boots</li>
                        <li>Running Shoes</li>
                        <li>Slippers</li>
                        <li>Hiking Shoes</li>
                        <li>Casual Loafers</li>
                    </ul>
                    <i class="fa fa-chevron-down down"></i>
                </li>
                <li class="listcategories-tem">Compression & Nike Pro
                    <ul>
                        <li>Nike Pro KIS-321</li>
                        <li>Nike Pro MK-1</li>
                        <li>Nike Pro IK-M</li>
                    </ul>
                    <i class="fa fa-chevron-down"></i>
                </li>
                <li class="listcategories-tem">Lifestyle Sneakers
                    <i class="fa fa-chevron-down"></i>
                </li>
                <li class="listcategories-tem">Shorts
                  
                </li>
                <li class="listcategories-tem">Shocks
                   
                </li>
            </ul>
        </div>
        <div class="content-products">
               <h2>SHOES PRODUCT</h2>
               <div class="content-products-list">
                   <div class="row">
                   <?php
                         $sql = 'select * from product';
                         $result = executeResult($sql);
                         $pages = ceil((sizeof($result))/8);
                         $current_page = 1;
                         if(isset($_GET['page'])){
                             $current_page = $_GET['page'];
                         }
                         $index = ($current_page-1)*8;
                         $sql = 'select * from product limit '.$index.' , 8';
                         $result = executeResult($sql);
                         foreach ($result as $product) {
                             echo ' <div class="content-product-info col-md-3">
                                        <img src="'."../images/".$product['thumbail'].'" alt="">
                                        <div class="product-info">
                                            <p>'.$product["title"].' <br> $'.$product["price"].' </p>
                                        </div>
                                    </div>';
                         }
                   ?>
                    </div>
                    <div class="row">
                        <ul class="pagination">
                            <?php
                                for ($i=1; $i <= $pages; $i++) {
                                    echo '<li><a href="?page='.$i.'&type='.$typeUser.'">'.$i.' </a></li>';
                                }
                            ?>
                        </ul>
                    </div>
            <div class="create-form">
                <div class="create-form-title">
                    <span cla>CREATE PRODUCT</span>
                </div>
                <div class="create-form-info">
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="" class="text-uppercase">Product Name: </label>
                            <input type="text" name="productname" id="">
                        </div>
                        <div class="form-group">
                            <label for="" class="text-uppercase">Product Price: </label>
                            <input type="text" name="productprice" id="">
                        </div>
                        <div class="create-image">
                            <label for="" class="text-uppercase"> Choose image: </label>
                            <input type="file" name="productimg" id="file">
                        </div>
                        <div class="create-btn">
                            <button class="btn btn-create align-justify-center btn-danger">Create</button>
                            <button class="btn btn-cancel align-justify-center btn-danger">Cancel</button>
                </div>
                </form>
                </div>  
            </div>

               </div>
       <div class="content-products-bestsell">
        <h2>Best Sellers </h2>
            <i class="fa fa-chevron-left prev"></i>
         <i class="fa fa-chevron-right next"></i>
        <div class="for_slick_slider multiple-items">
         <div class="items"><img src="../images/nike1.png" alt="">
            <p>Nike Air Zoom 33 <br> $120 </p>    
            <i class="fa fa-hear
            t"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
        </div>
         <div class="items"><img src="../images/nike2.png" alt="">
            <p>Nike Air Zoom 33 <br> $120 <br></p>  
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
        </div>
         <div class="items"><img src="../images/nike3.png" alt="">
            <p>Nike Air Zoom 33 <br> $120 </p>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i></div>
         <div class="items"><img src="../images/nike4.png" alt="">
            <p>Nike Air Zoom 33 <br> $120 </p>   
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i></div>
         <div class="items"><img src="../images/nike2.png" alt="">
            <p>Nike Air Zoom 33 <br> $120 </p> 
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i> </div>
         <div class="items"><img src="../images/nike3.png" alt="">
            <p>Nike Air Zoom 33 <br> $120 </p>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i>
            <i class="fa fa-heart"></i> </div>
        </div>
         </div>
       </div>
     
    </div>

    <script src="../js/jquery-3.5.1.min.js"></script>
     <script src="../js/jquery.js"> </script> 
    <script>   
        $(document).ready(function(){
            $('.btn-create').click(function(){
               //Lấy ra files
           
               var file_data = $('#file').prop('files')[0];

               var form_data = new FormData();
            //thêm files vào trong form data
               form_data.append('file', file_data);
               form_data.append('productname', $('input[name=productname]').val());
               form_data.append('productprice', $('input[name=productprice]').val());
               form_data.append('productimg', $('input[name=productimg]').val());
                $.ajax({
                    type: "post",
                    url : "ajax.php",
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data:   form_data,
                    success: function(msg){
                        alert(msg);
                        location.reload();
                    },
                })

            });
            var typeUser = <?php echo $typeUser; ?>;
            $('.create-link').click(function(){
                if(typeUser == '0')
                    alert("You can't create products");
                else{
                    $('.create-form').css("display","block");
                 }
            });
        
            $('.edit-link').click(function(){
               if(typeUser == '0'){
                    alert("You can't edit products");
               }
               else{
                    window.location.href = 'edit.php?type=1';
               }
            });
            $('.delete-link').click(function(){
               if(typeUser == '0'){
                    alert("You can't delete products");
               }
               else{
                window.location.href = 'delete.php?type=1';
                }
            });
        });
    </script>
    <script type="text/javascript" src="../js/slick.min.js"></script>
    </body>
</html>