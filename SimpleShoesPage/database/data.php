<?php
   require_once('define.php');
    function createDatabase(){
        $connect = new mysqli(HOST, USERNAME, '');
        mysqli_set_charset($connect, 'utf8');
        $sql = 'create database if not exists '.DATABASE;
        mysqli_query($connect, $sql);
        require_once 'sql_close.php';
    }

    function createTales(){
      require_once 'sql_connect.php';
        $sql = 'create table if not exists ';
        $tables = array (
          'user(
              id int primary key auto_increment,
              username varchar(30) not null,
              password varchar(15) not null,
              typeuser bit default 0 not null
          )',
          
          'product (
              id int primary key auto_increment,
              thumbail varchar(200) not null,
              title varchar(100) not null,
              price int default 0 not null
          )'
          );
          for ($i=0; $i < sizeof($tables); $i++) { 
              mysqli_query($connect, $sql.$tables[$i]);
          }
          require_once 'sql_close.php';
    }
    function createDataUser(){
      require_once 'sql_connect.php';
        $sql = 'insert into user (username, password, typeuser)';
        $data = array(
          'values ("admin", "123", 1)',
          'values ("client", "123", 0)'
        );
        for ($i=0; $i < sizeof($data); $i++) { 
        mysqli_query($connect, $sql.$data[$i]);
      }
      require_once 'sql_close.php';
    }
    function createDataProduct(){
      require_once 'sql_connect.php';
        $sql = 'insert into product (thumbail, title, price)';
        $data = array(
          'values ("nike1.png", "Nike Air Zoom 33", 130)',
          'values ("nike2.png", "Nike Air Zoom 33", 130)'
        );
        for ($i=0; $i < sizeof($data); $i++) { 
          mysqli_query($connect, $sql.$data[$i]);
        }
        require_once 'sql_close.php';
    }

    //run line by line
    //  createDatabase();
    //  createTales();
    //  createDataUser();
    // createDataProduct();
    

?>