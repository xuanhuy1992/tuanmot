$(document).ready(function(){

    $('.multiple-items').slick({
        autoplay:true,
        autoplaySpeed:2000,
        slidesToShow:3,
        slidesToScroll:1,  
        nextArrow: $('.next'),
        prevArrow: $('.prev'),
       
    });
   
    $('.btn-cancel').click(function(){
        $('.create-form').css("display","none");
    });
});
    
